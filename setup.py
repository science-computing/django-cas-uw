import os

from setuptools import setup, find_packages

version = '1.4.2'


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(name='django-cas-uw',
      version=version,
      description="Django Cas Client UW Local Fork",
      long_description=read('README.md'),
      classifiers=[
          "Development Status :: 5 - Production/Stable",
          "Environment :: Web Environment",
          "Framework :: Django",
          "Framework :: Django :: 1.8",
          "Framework :: Django :: 1.9",
          "Intended Audience :: Developers",
          "Natural Language :: English",
          "Operating System :: OS Independent",
          "Programming Language :: Python",
          "Programming Language :: Python :: 3",
          "Programming Language :: Python :: 2",
          "Topic :: Internet :: WWW/HTTP :: Dynamic Content :: CGI Tools/Libraries",
          "Topic :: Utilities",
          "License :: OSI Approved :: MIT License",
          ],
      keywords='django cas SSO',
      author='Derek Stegelman, Garrett Pennington, Tadonis',
      author_email='mvucicev@uwaterloo.ca',
      url='https://git.uwaterloo.ca/science-computing/django-cas-uw',
      license='MIT',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=True,
      )
